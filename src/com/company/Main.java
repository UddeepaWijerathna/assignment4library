package com.company;

import com.company.bo.Library;
import com.company.dao.LibraryDao;

public class Main {

    public static void main(String[] args) {
        LibraryDao library = new LibraryDao();
        library.addLibrary(new Library("main library", 1));
    }
}
