package com.company.service.impl;

import com.company.bo.*;
import com.company.dao.*;
import com.company.service.LibraryRepositoryService;
import com.company.service.impl.AdminServiceImpl;

public class LibraryRepositoryServiceImpl implements LibraryRepositoryService {


    AdminDao adminDao;
    LibraryDao libraryDao;
    StudentDao studentsDao;
    LibraryBookRecordDao libraryBookRecordDao;
    IssuedBookRecordDao issueBookRecordDao;

    public LibraryRepositoryServiceImpl() {
        adminDao = AdminDao.getInstance();
        libraryDao = LibraryDao.getInstance();
        studentsDao = StudentDao.getInstance();
        libraryBookRecordDao = LibraryBookRecordDao.getInstance();
        issueBookRecordDao = IssuedBookRecordDao.getInstance();
    }

    private boolean validateBookThatIsGoingToReturn(Student student, Book book, Admin admin, Library library) {

        return libraryDao.isLibraryValid(library) && studentsDao.isStudentValid(student)
                && adminDao.isAdminValid(admin) ;
    }

    @Override
    public void issueBookService(Student student, Book book, Admin admin, Library library) {

    }

    @Override
    public void returnBookService(Student student, Book book, Admin admin, Library library) {

    }
}
