package com.company.service;

import com.company.bo.Admin;
import com.company.bo.Book;
import com.company.bo.Library;
import com.company.bo.Student;

public interface LibraryRepositoryService {

    void issueBookService(Student student, Book book, Admin admin, Library library);
    void returnBookService(Student student, Book book, Admin admin, Library library);
}
