package com.company.bo;

import java.util.Objects;

public class LibraryBookRecord {
    private int libraryBookRecordId;
    private final Library library;
    private final Book book;
    private final int totalCount;
    private int issuedBookCount;

    public LibraryBookRecord(int libraryBookRecordId,Book book,Library library, int totalCount) {
        this.book=book;
        this.library=library;
        this.totalCount=totalCount;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LibraryBookRecord)) return false;
        LibraryBookRecord that = (LibraryBookRecord) o;
        return getLibraryBookRecordId() == that.getLibraryBookRecordId() &&
                getTotalCount() == that.getTotalCount() &&
                Objects.equals(getLibrary(), that.getLibrary()) &&
                Objects.equals(getBook(), that.getBook());
    }

    public Book getBook() {
        return book;
    }

    public Library getLibrary() {
        return library;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public int getLibraryBookRecordId() {
        return libraryBookRecordId;
    }

    public int getIssuedBookCount() {
        return issuedBookCount;
    }
}
