package com.company.bo;

import java.util.Objects;

public class Admin {
    private final int identityNumber;
    private final String name;

    public Admin(String name, int identityNumber) {
        this.name=name;
        this.identityNumber=identityNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Admin)) return false;
        Admin admin = (Admin) o;
        return getIdentityNumber() == admin.getIdentityNumber() &&
                Objects.equals(getName(), admin.getName());
    }



    public String getName() {
        return name;
    }

    public int getIdentityNumber() {
        return identityNumber;
    }
}
