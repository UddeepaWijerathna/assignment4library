package com.company.bo;

import com.company.bo.enums.Degree;

import java.util.Objects;

public class Student {

    private int rollNumber;
    private String name;
    private String batch;
    private char section;
    private Degree degree;

    public Student(int rollNumber, String name, String batch, char section, Degree degree) {
        this.rollNumber=rollNumber;
        this.name=name;
        this.batch=batch;
        this.section=section;
        this.degree=degree;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;
        Student student = (Student) o;
        return getRollNumber() == student.getRollNumber() &&
                getSection() == student.getSection() &&
                Objects.equals(getName(), student.getName()) &&
                Objects.equals(getBatch(), student.getBatch()) &&
                getDegree() == student.getDegree();
    }

    public int getRollNumber() {
        return rollNumber;
    }

    public String getName() {
        return name;
    }

    public String getBatch() {
        return batch;
    }

    public char getSection() {
        return section;
    }

    public Degree getDegree() {
        return degree;
    }


}
