package com.company.bo;

import java.util.Objects;

public class IssuedBookRecord {
    private final Book book;
    private final Student student;
    private final Admin admin;
    private int issuedBookId =0;
    private static int autoIncId = 0;

    public IssuedBookRecord(Book book, Student student, Admin admin) {
        this.book = book;
        this.student = student;
        this.admin = admin;
        this.issuedBookId=autoIncrementId();


    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IssuedBookRecord)) return false;
        IssuedBookRecord that = (IssuedBookRecord) o;
        return  Objects.equals(getBook(), that.getBook()) &&
                Objects.equals(getStudent(), that.getStudent()) &&
                Objects.equals(getAdmin(), that.getAdmin());
    }

    public Book getBook() {
        return book;
    }

    public Student getStudent() {
        return student;
    }

    public Admin getAdmin() {
        return admin;
    }

    public int autoIncrementId() {
        return ++autoIncId;
    }

    public int getIssuedBookId() {
        return issuedBookId;
    }
}
