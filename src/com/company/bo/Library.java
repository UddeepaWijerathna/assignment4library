package com.company.bo;

import java.util.Objects;

public class Library {
    private final String name;
    private final int libraryId;

    public Library(int libraryId,String name) {

        this.libraryId=libraryId;
        this.name=name;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Library)) return false;
        Library library = (Library) o;
        return getLibraryId() == library.getLibraryId() &&
                Objects.equals(getName(), library.getName());
    }

    public String getName() {
        return name;
    }

    public int getLibraryId() {
        return libraryId;
    }

}
