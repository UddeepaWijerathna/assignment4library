package com.company.bo;

import com.company.bo.enums.Subject;

import java.util.Objects;

public class Book {
    private final int bookId;
    private final String name;
    private final Subject subject;

    public Book(String name, Subject subject, int bookId) {
        this.bookId=bookId;
        this.name=name;
        this.subject=subject;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;
        Book book = (Book) o;
        return getBookId() == book.getBookId() &&
                Objects.equals(getName(), book.getName()) &&
                getSubject() == book.getSubject();
    }

    public int getBookId() {
        return bookId;
    }

    public String getName() {
        return name;
    }

    public Subject getSubject() {
        return subject;
    }

}
