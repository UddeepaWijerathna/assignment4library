package com.company.bo.enums;

public enum Subject {
    MATH, PHYSICS, CHEMISTRY, BIO
}
