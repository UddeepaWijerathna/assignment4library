package com.company.dao;

import com.company.bo.Admin;
import com.company.bo.Book;
import com.company.bo.IssuedBookRecord;
import com.company.bo.Student;

import java.util.ArrayList;
import java.util.List;

public class IssuedBookRecordDao {
    private List<IssuedBookRecord> issuedBookRecords;

    private static IssuedBookRecordDao issuedBookRecordDao;

    private IssuedBookRecordDao() {
        this.issuedBookRecords = new ArrayList<IssuedBookRecord>();
    }

    //TODO give student and take the issued book count for the student
    public static IssuedBookRecordDao getInstance() {
        issuedBookRecordDao = issuedBookRecordDao == null ? new IssuedBookRecordDao() : issuedBookRecordDao;
        return issuedBookRecordDao;
    }


    public void addIssuedBookRecord(IssuedBookRecord issuedBookRecord) {
        issuedBookRecords.add(issuedBookRecord);
    }

    public void removeIssuedBookRecord(IssuedBookRecord issuedBookRecord) {
        issuedBookRecords.remove(issuedBookRecord);
    }

    public IssuedBookRecord findIssuedBookRecord(String bookName) {
        IssuedBookRecord issuedBookRecord = null;
        for (int index = 0; index <= issuedBookRecords.size(); index++) {
            issuedBookRecord = issuedBookRecords.get(index);
            if (issuedBookRecord.getBook().getName() == bookName)
                break;
        }

        return issuedBookRecord;
    }

    public void updateIssuedBookRecord(int issuedBookId, Student student, Book book, Admin admin) {
        for (int index = 0; index <= issuedBookRecords.size(); index++) {
            if (issuedBookRecords.get(index).getIssuedBookId() == issuedBookId)
                issuedBookRecords.set(index, new IssuedBookRecord(book, student, admin));
        }

    }


}
