package com.company.dao;

import com.company.bo.Book;
import com.company.bo.Library;
import com.company.bo.LibraryBookRecord;

import java.util.ArrayList;
import java.util.List;

public class LibraryBookRecordDao {
    private List<LibraryBookRecord> libraryBookRecords;

    private static LibraryBookRecordDao libraryBookRecordDao;

    private LibraryBookRecordDao() {
        this.libraryBookRecords = new ArrayList<LibraryBookRecord>();
    }

    public static LibraryBookRecordDao getInstance() {
        libraryBookRecordDao = libraryBookRecordDao == null ? new LibraryBookRecordDao() : libraryBookRecordDao;
        return libraryBookRecordDao;
    }


    public void addLibraryBookRecord(LibraryBookRecord libraryBookRecord) {
        libraryBookRecords.add(libraryBookRecord);
    }

    public void removeLibraryBookRecord(LibraryBookRecord libraryBookRecord) {
        libraryBookRecords.remove(libraryBookRecord);
    }

    public LibraryBookRecord findLibraryBookRecord(int libraryBookRecordId) {
        LibraryBookRecord libraryBookRecord = null;
        for (int index = 0; index <= libraryBookRecords.size(); index++) {
            libraryBookRecord = (LibraryBookRecord) libraryBookRecords.get(index);
            if (libraryBookRecord.getLibraryBookRecordId() == libraryBookRecordId)
                break;
        }

        return libraryBookRecord;
    }

    public void updateLibraryBookRecord(int libraryBookRecordId, Book book, Library library, int totalCount) {
        for (int index = 0; index <= libraryBookRecords.size(); index++) {
            if (libraryBookRecords.get(index).getLibraryBookRecordId() == libraryBookRecordId)
                new LibraryBookRecord(libraryBookRecordId, book, library, totalCount);
        }

    }

}
