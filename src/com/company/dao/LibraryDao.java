package com.company.dao;

import com.company.bo.Library;

import java.util.ArrayList;
import java.util.List;

public class LibraryDao {
    private List<Library> libraries;

    private static LibraryDao libraryDao;

    private LibraryDao() {
        this.libraries = new ArrayList<Library>();
    }

    public static LibraryDao getInstance() {
        libraryDao = libraryDao == null ? new LibraryDao() : libraryDao;
        return libraryDao;
    }

    public void addLibrary(Library library) {
        libraries.add(library);
    }

    public void removeLibrary(Library library) {
        libraries.remove(library);
    }

    public Library findLibrary(int libraryNumber) {
        Library library = null;
        for (int index = 0; index <= libraries.size(); index++) {
            library = (Library) libraries.get(index);
            if (library.getLibraryId() == libraryNumber)
                break;
        }

        return library;
    }

    public void updateLibrary(int libraryId, String name) {
        for (int index = 0; index <= libraries.size(); index++) {
            if (libraries.get(index).getLibraryId() == libraryId)
                libraries.set(index,new Library(libraryId,name));
        }

    }


    public boolean isLibraryValid(Library library) {
        if(libraries.contains(library))
            return true;
        else
            return false;
    }
}
