package com.company.dao;

import com.company.bo.Book;
import com.company.bo.enums.Subject;

import java.util.ArrayList;
import java.util.List;

public class BookDao {
    private List<Book> books;

    private static BookDao bookDao;

    private BookDao(){
        this.books = new ArrayList<Book>();
    }

    public static BookDao getInstance(){
        bookDao = bookDao== null? new BookDao(): bookDao;
        return bookDao;
    }

    public void addBook(Book book){
        books.add(book);
    }

    public void removeBook(Book book){
        books.remove(book);
    }

    public Book findBook(int bookNumber){
        Book book=null;
        for(int index=0;index<=books.size();index++){
            book = (Book) books.get(index);
            if(book.getBookId()==bookNumber)
                break;
        }

        return book;
    }

    public void updateBook(int bookId, String name, Subject subject){
        for(int index=0;index<=books.size();index++){
            if(books.get(index).getBookId()==bookId)
                books.set(index,new Book(name,subject,bookId));
        }

    }

    public boolean isBookValid(Book book){
        if(books.contains(book))
            return true;
        else
            return false;
    }

}
