package com.company.dao;

import com.company.bo.Student;
import com.company.bo.enums.Degree;

import java.util.ArrayList;
import java.util.List;

public class StudentDao {
    private List<Student> students;

    private static StudentDao studentDao;

    public StudentDao() {
        this.students = new ArrayList<Student>();
    }

    public static StudentDao getInstance(){
        studentDao = studentDao== null? new StudentDao(): StudentDao.studentDao;
        return studentDao;
    }

    public void addStudent(Student student) {
        students.add(student);
    }

    public void removeStudent(Student student) {
        students.remove(student);
    }

    public Student findStudent(int rollNumber) {
        Student student = null;
        for (int index = 0; index <= students.size(); index++) {
            student = (Student) students.get(index);
            if (student.getRollNumber() == rollNumber)
                break;
        }

        return student;
    }

    public void updateStudent(int rollNumber, String name, String batch, char section, Degree degree) {
        for (int index = 0; index <= students.size(); index++) {
            if (students.get(index).getRollNumber() == rollNumber)
                students.set(index, new Student(rollNumber, name, batch, section, degree));
        }

    }

    public boolean isStudentValid(Student student){
        if(students.contains(student))
            return true;
        else
            return false;
    }


}
