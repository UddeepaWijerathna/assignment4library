package com.company.dao;

import com.company.bo.Admin;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AdminDao {
    private List<Admin> admins;

    private static AdminDao adminDao;

    private AdminDao() {
        this.admins = new ArrayList<Admin>();
    }

    public static AdminDao getInstance() {
        adminDao = adminDao == null ? new AdminDao() : adminDao;
        return adminDao;
    }

    public void addAdmin(Admin admin) {
        admins.add(admin);
    }


    public void removeAdmin(Admin admin) {
        admins.remove(admin);
    }

    public Admin findAdmin(int identityNumber) {
        Admin admin = null;
        for (int index = 0; index <= admins.size(); index++) {
            admin = admins.get(index);
            if (admin.getIdentityNumber() == identityNumber)
                break;
        }

        return admin;
    }


    public void updateAdmin(int identityNumber, String name) {
        for (int index = 0; index <= admins.size(); index++)
            if (admins.get(index).getIdentityNumber() == identityNumber)
                admins.set(index, new Admin(name, identityNumber));
    }

    public boolean isAdminValid(Admin admin){
        if(admins.contains(admin))
            return true;
        else
            return false;
    }

}




